variable "email" {}
variable "api_key" {}
variable "public_ip" {}
variable "zone_id" {}

provider "cloudflare" {
  email = var.email
  api_key = var.api_key
}

terraform {
  backend "http" {}
}

module "todo_worker" {
  source      = "../../../modules/cloudflare-worker"
  zone_id     = var.zone_id
  name        = "todo"
  public_ip   = var.public_ip
  file        = file("todo.js")
  namespace_binding_name = "IMPORTANT_TODOS"
}
