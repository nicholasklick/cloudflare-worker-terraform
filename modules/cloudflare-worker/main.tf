# DNS Records
resource "cloudflare_record" "worker_record" {
  zone_id = var.zone_id
  name    = var.name
  value   = var.public_ip
  type    = "A"
  proxied = true
}

# Worker KV
resource "cloudflare_workers_kv_namespace" "worker_namespace" {
  title = var.name
}

# Worker Script
resource "cloudflare_worker_script" "worker_script" {
  name = var.name
  content = var.file

  kv_namespace_binding {
    name = var.namespace_binding_name
    namespace_id = cloudflare_workers_kv_namespace.worker_namespace.id
  }
}

# Worker Route
resource "cloudflare_worker_route" "worker_route" {
  zone_id = var.zone_id
  pattern = "todo.nklick.com"
  script_name = cloudflare_worker_script.worker_script.name
}
